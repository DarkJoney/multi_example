import os


def create_folder():
    path = os.getcwd() + '\pics'

    try:
        os.mkdir(path)
    except OSError:
        print("Creation of the directory %s failed" % path)
    else:
        print("Successfully created the directory %s " % path)


def delete_folder():
    path = os.getcwd() + '\pics'

    try:
        os.rmdir(path)
    except OSError:
        print("Deletion of the directory %s failed" % path)
    else:
        print("Successfully deleted the directory %s" % path)
