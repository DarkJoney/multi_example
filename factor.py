import time
from multiprocessing.pool import ThreadPool
import pandas as pd
import multiprocessing

def factorize_naive(n):
    """ A naive factorization method. Take integer 'n', return list of
        factors.
    """
    if n < 2:
        return []
    factors = []
    p = 2

    while True:
        if n == 1:
            return factors

        r = n % p
        if r == 0:
            factors.append(p)
            n = n / p
        elif p * p >= n:
            factors.append(n)
            return factors
        elif p > 2:
            # Advance in steps of 2 over odd numbers
            p += 2
        else:
            # If p == 2, get to 3
            p += 1
    assert False, "unreachable"


def factorize(range_number):
    result = {}
    data = [n for n in range(range_number)]
    for n in data:
        result[n] = factorize_naive(n)
    return result


print("Yours CPU has " + str(multiprocessing.cpu_count()) + " cores")
print('How much workers do you want to use?')
iterations = input()

if int(iterations) < 1:
    iterations = 1

iterations = int(iterations)
workers = 1
DATA_SIZE = 1000000
measurements = []

for i in range(iterations):
    print("Processing with: " + str(workers) + " workers")
    start = time.time()
    with ThreadPool(workers) as pool:
        input_data = [DATA_SIZE // workers for _ in range(workers)]
        pool.map(factorize, input_data)
    stop = time.time()
    measurements.append(stop - start)
    workers = workers + 1


print(measurements)
indexes = []
for i in range(len(measurements)):
    indexes.append(i+1)

df = pd.DataFrame({'Workers': indexes,
                   'Execution time': measurements},)

writer = pd.ExcelWriter('results/factor.xlsx', engine='xlsxwriter')
df.at['C', '1'] = 'CPU Cores: ' + str(multiprocessing.cpu_count())
df.to_excel(writer, sheet_name='Sheet1', index=False)
workbook = writer.book
worksheet = writer.sheets['Sheet1']
chart = workbook.add_chart({'type': 'line'})
chart.add_series({'values': '=Sheet1!$B$2:$B$'+str(workers)})
chart.set_x_axis({'name': 'Index', 'position_axis': 'on_tick'})
chart.set_y_axis({'name': 'Value', 'major_gridlines': {'visible': False}})
chart.set_legend({'position': 'none'})
worksheet.insert_chart('D2', chart)
writer.save()

print("Done! Check the results folder for results.")