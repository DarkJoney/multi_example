import time
import threading
import multiprocessing
import itertools
import os
import logging
import random
import string
import requests
from functools import partial
from multiprocessing import Queue
from multiprocessing.pool import ThreadPool
import utils
import pandas as pd

class timer():
    def __init__(self, message):
        self.message = message

    def __enter__(self):
        self.start = time.time()
        return None

    def __exit__(self, type, value, traceback):
        elapsed_time = (time.time() - self.start)
        print(self.message.format(elapsed_time))


workers = 1
DATA_SIZE = 100


def fetch_pic(num_pic):
    url = 'https://picsum.photos/400/600'
    path = 'pics'
    for _ in range(num_pic):
        random_name = ''.join(random.choices(string.ascii_letters + string.digits, k=5))
        response = requests.get(url)
        if response.status_code == 200:
            with open(f'{path}/{random_name}.jpg', 'wb') as f:
                f.write(response.content)
                print(f"Fetched pic [{os.getpid()}]: {f.name}")


print("Yours CPU has " + str(multiprocessing.cpu_count()) + " cores")
print('How much workers do you want to use?')
iterations = input()

if int(iterations) < 1:
    iterations = 1

iterations = int(iterations)
measurements = []

utils.create_folder()

for i in range(iterations):
    print("Processing with: " + str(workers) + " workers")
    start = time.time()
    with ThreadPool(workers) as pool:
        input_data = [DATA_SIZE // workers for _ in range(workers)]
        pool.map(fetch_pic, input_data)
    stop = time.time()
    measurements.append(stop - start)
    workers = workers + 1

utils.delete_folder()
print(measurements)
indexes = []
for i in range(len(measurements)):
    indexes.append(i+1)

df = pd.DataFrame({'Workers': indexes,
                   'Execution time': measurements},)

writer = pd.ExcelWriter('results/pics.xlsx', engine='xlsxwriter')
df.at['C', '1'] = 'CPU Cores: ' + str(multiprocessing.cpu_count())
df.to_excel(writer, sheet_name='Sheet1', index=False)
workbook = writer.book
worksheet = writer.sheets['Sheet1']
chart = workbook.add_chart({'type': 'line'})
chart.add_series({'values': '=Sheet1!$B$2:$B$'+str(workers)})
chart.set_x_axis({'name': 'Index', 'position_axis': 'on_tick'})
chart.set_y_axis({'name': 'Value', 'major_gridlines': {'visible': False}})
chart.set_legend({'position': 'none'})
worksheet.insert_chart('D2', chart)
writer.save()

print("Done! Check the results folder for results.")